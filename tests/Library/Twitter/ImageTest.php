<?php
use Library\Twitter\Image;

class ImageTest extends PHPUnit_Framework_TestCase
{
    CONST SUCCESS_IMAGE = "http://pbs.twimg.com/profile_images/477648908915445760/0BZduvKx.jpeg";

    public $image;

    public function testGet()
    {
        $this->assertNotFalse(Image::get(self::SUCCESS_IMAGE));
    }

    public function testGetType()
    {
        $content_type = Image::getType("https://pbs.twimg.com/profile_images/589401952242401280/XtS796kB.jpg");
        $this->assertEquals("image/jpeg", $content_type);
    }

    public function 
}
