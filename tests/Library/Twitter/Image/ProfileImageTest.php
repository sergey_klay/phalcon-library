<?php
use Library\Twitter\Image\ProfileImage;

class ProfileImageTest extends PHPUnit_Framework_TestCase
{
    CONST SUCCESS_IMAGE = "http://pbs.twimg.com/profile_images/477648908915445760/0BZduvKx_normal.jpeg";
    CONST FAILED_IMAGE  = "http://pbs.twimg.com/477648908915445760/0BZduvKx.jpeg:small";

    public function testCheckImageSuccessCase()
    {
        $this->assertTrue(ProfileImage::check(self::SUCCESS_IMAGE));
    }

    public function testCheckImageFailedCase()
    {
        $this->assertNotTrue(ProfileImage::check(self::FAILED_IMAGE));
    }

    public function testGetImageSuccessCase()
    {
        try {
            $image = ProfileImage::get(self::SUCCESS_IMAGE);
            $this->assertTrue(true);
        } catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     *
     */
    public function testGetImageFailedCase()
    {
        try {
            $image = ProfileImage::get(self::FAILED_IMAGE);
            $this->fail("Not Call Exception");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }

    public function testGetOriginalUrl()
    {
        $this->assertEquals("http://pbs.twimg.com/profile_images/477648908915445760/0BZduvKx.jpeg", ProfileImage::getOriginalUrl(self::SUCCESS_IMAGE));
    }
}
