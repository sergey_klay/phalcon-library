<?php
use Library\Twitter\Image\TwitImage;

class TwitImageTest extends PHPUnit_Framework_TestCase
{
    CONST SUCCESS_IMAGE = "https://pbs.twimg.com/media/CDU5w_6UUAApA96.jpg:small";
    CONST FAILED_IMAGE  = "https://ps.twimg.com/media/CDU5w_6pA96.jpg:tall";

    public function testCheckImageSuccessCase()
    {
        $this->assertTrue(TwitImage::check(self::SUCCESS_IMAGE));
    }

    public function testCheckImageFailedCase()
    {
        $this->assertNotTrue(TwitImage::check(self::FAILED_IMAGE));
    }

    public function testGetImageSuccessCase()
    {
        try {
            $image = TwitImage::get(self::SUCCESS_IMAGE);
            $this->assertTrue(true);
        } catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     *
     */
    public function testGetImageFailedCase()
    {
        try {
            $image = TwitImage::get(self::FAILED_IMAGE);
            $this->fail("Not Call Exception");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }
}
