<?php
use Library\Twitter\Image\Twipple;

class TwippleTest extends PHPUnit_Framework_TestCase
{
    CONST SUCCESS_IMAGE = "http://p.twipple.jp/AWYL3";
    CONST FAILED_IMAGE  = "http://d.twipple.jp/AWYL3";

    public function testCheckImageSuccessCase()
    {
        $this->assertTrue(Twipple::check(self::SUCCESS_IMAGE));
    }

    public function testCheckImageFailedCase()
    {
        $this->assertNotTrue(Twipple::check(self::FAILED_IMAGE));
    }

    public function testGetImageSuccessCase()
    {
        try {
            $image = Twipple::get(self::SUCCESS_IMAGE);
            $this->assertTrue(true);
        } catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     *
     */
    public function testGetImageFailedCase()
    {
        try {
            $image = Twipple::get(self::FAILED_IMAGE);
            $this->fail("Not Call Exception");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }

    public function testGetOriginalUrl()
    {
        $this->assertEquals("http://p.twpl.jp/show/large/AWYL3", Twipple::getOriginalUrl(self::SUCCESS_IMAGE));
    }
}
