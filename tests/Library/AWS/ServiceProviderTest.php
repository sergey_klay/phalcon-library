<?php
use Library\AWS\ServiceProvider,
    Library\AWS\Services\S3;

class SerivceProviderTest extends PHPUnit_Framework_TestCase
{
    public function testProvidedService()
    {
        $aws = new ServiceProvider($_ENV['AWS_ACCESS_KEY'],$_ENV['AWS_SECRET_KEY']);
        $this->assertEquals("Aws\S3\S3Client",get_class($aws->getInstance(new S3)));
    }
}
