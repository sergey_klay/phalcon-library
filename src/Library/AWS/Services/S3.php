<?php
namespace Library\AWS\Services;
use \Aws\S3\S3Client as Client;
class S3 implements ServiceInterface
{
    /**
     * @var String AWS_Access_Key
     */
    protected $access_key;
    /**
     * @var String AWS_Access_Secret_Key
     */
    protected $secret_key;

    /**
     *	create new Instance S3Client
     *	@access public
     *	@param String AWS_Access_Key
     *	@param String AWS_Access_Secret_Key
     *	@param String region
     *	@return \Aws\S3\S3Client AWS-SDK-PHP\S3Client
     */
    public function getInstance($access_key, $secret_key, $region = "ap-northeast-1")
    {
        $params = array(
            'key'      => $access_key,
            'secret'   => $secret_key,
            'region'   => $region
        );
        return Client::factory($params);
    }
}
