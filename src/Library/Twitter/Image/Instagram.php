<?php
namespace Library\Twitter\Image;

class Instagram extends ImageServiceBaseClass
{
    public static function check($url)
    {
        return (bool)strpos($url,"instagram.com/p/");
    }

    public static function get($url)
    {
        $url = self::getOriginalUrl($url);
        return self::getImage($url);
    }

    public static function getOriginalUrl($url)
    {
        $img = "http://api.instagram.com/oembed?url={$url}";
        $json = json_decode(file_get_contents($img));
        return $json->thumbnail_url;
    }
}
