<?php
namespace Library\Twitter\Image;

class Twipple extends ImageServiceBaseClass
{
    public static function check($url)
    {
        return (bool)strpos($url,"p.twipple.jp");
    }

    public static function get($url)
    {
        $url = self::getOriginalUrl($url);
        return self::getImage($url);
    }

    public static function getOriginalUrl($url)
    {
        return str_replace('p.twipple.jp','p.twpl.jp/show/large', $url);
    }
}
