<?php
namespace Library\Twitter;
class Image
{
    public static function get($url)
    {
        switch(true) {
            case Image\ProfileImage::check($url):
                return Image\ProfileImage::get($url); break;
            case Image\TwitImage::check($url):
                return Image\TwitImage::get($url); break;
            case Image\Twipple::check($url):
                return Image\Twipple::get($url); break;
            case Image\Instagram::check($url):
                return Image\Instagram::get($url); break;
            default:
                return false;
        }
    }

    public static function isImage($url) {
        return (
            Image\ProfileImage::check($url) ||
            Image\TwitImage::check($url)    ||
            Image\Twipple::check($url)      ||
            Image\Instagram::check($url)
        );
    }

    public static function merge($entities)
    {
        $result_media = [];
        $result_urls  = [];
        if(!empty($entities->media)) {
            $result_media = array_map(function($image) {
                return (object)array(
                    "id"  => md5($image->id),
                    "url" => $image->media_url
                );
            }, $entities->media);
        }

        if(!empty($entities->urls)) {
            $result_urls  = array_map(function($url) {
                return (object)array(
                    "id"  => md5($url->expanded_url),
                    "url" => $url->expanded_url
                );
            }, $entities->urls);
        }

        return array_values(array_filter(array_merge($result_media, $result_urls), function($obj) {
            return self::isImage($obj->url);
        }));
    }

    public static function getType($url) {
        $contents = self::get($url);
        $finfo    = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_buffer($finfo, $contents);
        finfo_close($finfo);
        return $mimeType;
    }
}
