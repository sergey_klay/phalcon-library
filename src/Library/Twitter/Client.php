<?php
namespace Library\Twitter;
use \Abraham\TwitterOAuth\TwitterOAuth;
use \Exception;
use \Library\Twitter\Image\ProfileImage;
use \Library\Twitter\Image;
class Client
{

    /**
     * @var Abraham\TwitterOAuth\TwitterOAuth
     */
    protected $client;

    /**
     * @var Array
     */
    protected $users;

    public function __construct($key, $secret, $access_token, $token_secret)
    {
        $this->client = new TwitterOAuth($key, $secret, $access_token, $token_secret);
    }

    public function getUserID($screen_name)
    {
        $conditions = [
            "screen_name" => $screen_name
        ];
        $profile = $this->client->get("users/show", $conditions);
        if (200 === $this->client->getLastHttpCode()) {
            return (int)$profile->id;
        } else {
            throw new Exception("UserName Not Found!");
        }
    }

    public function isFollow($screen_name)
    {
        $conditions = [
            "screen_name" => $screen_name
        ];
        $status = $this->client->get("friendships/lookup", $conditions);
        if (200 === $this->client->getLastHttpCode()) {
            return (isset($status[0]->connections) && in_array("following", (array)$status[0]->connections));
        } else {
            throw new Exception("User Not Found!");
        }
    }

    public function getUserName($user_id)
    {
        $conditions = [
            "user_id" => $user_id
        ];
        $profile = $this->client->get("users/show", $conditions);
        if (200 === $this->client->getLastHttpCode()) {
            return $profile->screen_name;
        } else {
            throw new Exception("UserName Not Found!");
        }
    }

    /**
     * getTimeline
     * @param Integer $count
     * @param Integer $since_id
     */
    public function getTimeline($count = 200, $since_id = 1)
    {
        $conditions = [
            "count"     => (int)$count,
            "since_id"  => (int)$since_id,
            "trim_user" => 1
        ];
        $result = $this->client->get("statuses/home_timeline", $conditions);
        if(200 === $this->client->getLastHttpCode()) {
            return array_map(function($post) {
                return (object)[
                    'id'         => $post->id,
                    'user_id'    => $post->user->id,
                    'body'       => $post->text,
                    'created_at' => $post->created_at,
                    'image'      => Image::merge($post->entities),
                    'isreplay'  => (!empty($post->entities->user_mentions))
                ];
            }, $result);
        } else {
            throw new Exception("Timeline can not Get!");
        }
    }

    /**
     * addTwitterUser
     * @param String $user_name
     * @return Boolean
     */
    public function addUser($user_id)
    {
        $conditions = [
            "user_id" => $user_id
        ];
        $result = $this->client->post('friendships/create', $conditions);
        if (200 === $this->client->getLastHttpCode()) {
            return true;
        } else {
            throw new Exception("User can not Follow!");
        }
    }

    public function deleteUser($user_id)
    {
        $conditions = [
            "user_id" => $user_id
        ];
        $result = $this->client->post('friendships/destroy', $conditions);
        if(200 === $this->client->getLastHttpCode()) {
            return true;
        } else {
            throw new Exception("User can not Destroy!");
        }
    }

    /**
     * getUserFollowingUserProfiles
     * @param String $own_screen_name
     * @return Array
     */
    public function getFollowsProfile($own_screen_name)
    {
        $conditions = [
            'count' => 200,
            'screen_name' => $own_screen_name
        ];
        $result = $this->client->get('friends/list', $conditions);
        if(200 === $this->client->getLastHttpCode()) {
            return array_map(function($profile) {
                return (object)[
                    "id"          => $profile->id,
                    "screen_name" => $profile->screen_name,
                    "name"        => $profile->name,
                    "location"    => $profile->location,
                    "description" => $profile->description,
                    "image"       => ProfileImage::getOriginalUrl($profile->profile_image_url),
                    "created_at"  => strtotime($profile->created_at),
                ];
            },$result->users);
        } else {
            throw new Exception("Follow Ids can not Get!");
        }
    }

    /**
     * getUserProfile
     * @param Integer $user_id
     * @return String
     */
    public function getUserProfile($user_id)
    {
        $conditions = [
            "user_id" => $user_id
        ];
        $profile = $this->client->get("users/show", $conditions);
        if (200 === $this->client->getLastHttpCode()) {
            return (object)[
                "id"          => $profile->id,
                "screen_name" => $profile->screen_name,
                "name"        => $profile->name,
                "location"    => $profile->location,
                "description" => $profile->description,
                "image"       => ProfileImage::getOriginalUrl($profile->profile_image_url),
                "created_at"  => strtotime($profile->created_at),
            ];
        } else {
            throw new Exception("UserID Not Found!");
        }
    }

    public function getUserProfiles(array $user_ids)
    {
        if(100  < count($user_ids)) {
            throw new Exception("get Profile Limit <= 100 !");
        }
        $conditions = [
            "user_id" => implode(",", $user_ids)
        ];
        $profiles = $this->client->get("users/lookup", $conditions);
        if(200 === $this->client->getLastHttpCode()) {
            return array_map(function($profile) {
                return (object)[
                    "id"          => $profile->id,
                    "screen_name" => $profile->screen_name,
                    "name"        => $profile->name,
                    "location"    => $profile->location,
                    "description" => $profile->description,
                    "image"       => ProfileImage::getOriginalUrl($profile->profile_image_url),
                    "created_at"  => strtotime($profile->created_at),
                ];
            }, $profiles);
        } else {
            throw new Exception("UserProfiles can not Get!");
        }
    }

}
