<?php
namespace Library\Sitemap;
use \SimpleXMLElement;

/**
 * PhalconMVC用サイトマップ作成クラス
 */
class Generator{

    /**
     * @access protected
     * @var SimpleXMLElement
     */
    protected $xml;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></urlset>');
    }

    /**
     * アイテム作成用メソッド
     * @access public
     * @depends Item
     * @return Item
     */
    public function createItem()
    {
        return new Item($this->xml->addChild("url"));
    }

    /**
     * XML文字列作成用メソッド
     * @access public
     * @return string
     */
    public function generate()
    {
        return $this->xml->asXML();
    }

    public function __toString()
    {
        return $this->xml;
    }
}
