<?php
namespace Library\Adapter;

use \Phalcon\Mvc\Model\Criteria as Query;
/**
 * PhalconMVC Model用ページネーター作成クラス
 */

class Paginator
{
    /**
     * QueryModel
     * @var \Phalcon\Mvc\Model\Criteria
     */
    protected $query;

    /**
     * CurrentPageNumber
     * @var integer
     */
    protected $current;

    /**
     * LastPageNumber
     * @var integer
     */
    protected $last;

    /**
     * Limit PagePer
     * @var  integer
     */
    protected $limit;

    /**
     * OffsetField
     * @var integer
     */
    protected $offset;

    /**
     * CountAllField
     * @var integer
     */
    protected $count;

    /**
     * ResultSet
     * @var Phalcon\Mvc\Model\Resultset\Simple
     */
    protected $items;

    /**
     * コンストラクタ
     * @access public
     * @param \Phalcon\Mvc\Model\Criteria $query
     * @param integer $page
     * @param integer $limit
     * @return void
     */
    public function __construct(Query $query, $page = null, $limit = 10)
    {
        $this->query   = $query;
        $this->current = ($page) ? (int)$page : (int)$_GET['page'];
        $this->limit   = (int)$limit;
    }

    /**
     * ページ関連情報取得用メソッド
     * @access private
     * @return void
     */
    private function getPage()
    {
        $query = clone $this->query;
        $this->count = count($query->columns(array("id"))->execute());
        $this->last = ceil($this->count / $this->limit);
    }

    /**
     * オフセット情報取得用メソッド
     * @access private
     * @return void
     */
    private function getOffset()
    {
        $this->offset = ($this->current - 1) * $this->limit;
    }

    /**
     * 前ページ番号取得用メソッド
     * @access private
     * @return void
     */
    private function getBefore()
    {
        $this->before = ($this->current <= 1) ? 1 : $this->current - 1;
    }

    /**
     * 次ページ番号取得用メソッド
     * @access private
     * @return void
     */
    private function getAfter()
    {
        $this->after = ($this->last <= $this->current) ? $this->last : $this->current + 1;
    }

    /**
     * クエリ取得用メソッド
     * @access private
     * @return void
     */
    private function getData()
    {
        $this->items = $this->query->limit($this->limit,$this->offset)->execute();
    }

    /**
     * ページネート取得用メソッド
     * @access public
     * @return stdClass
     */
    public function getPaginate()
    {
        $this->getPage();
        $this->getOffset();
        $this->getBefore();
        $this->getAfter();
        $this->getData();
        return (object)array(
            "items"    => $this->items,
            "count"   => $this->count,
            "current" => $this->current,
            "first"   => 1,
            "last"    => $this->last,
            "before"  => $this->before,
            "after"   => $this->after,
            "prev"    => $this->before,
            "next"    => $this->after
        );
    }
}
